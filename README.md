<!-- (c) https://github.com/MontiCore/monticore -->
# viewverficiation-visualization-integrationtests

structure looks like https://github.com/EmbeddedMontiArc/Tagging-Examples

use Jasmine https://jasmine.github.io/ test framework to check whether the visualization draw the components (test if svg field with the corresponding name exists), ports and connectors needed.
Only test of existence of these elements, do not test of layout; as this test should also be valid when updating the layout algorithm.
